#ifndef SWFTypes_h__
#define SWFTypes_h__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define isLittleEndian static_cast<bool>(static_cast<unsigned char>(0x0001))
#define reverse_2(x) (isLittleEndian ? (x) : ((((x)&0xFF)<<8)|(((x)>>8)&0xFF)))
#define reverse_4(x) (isLittleEndian ? (x) : ((((x)&0xFF)<<24)|((((x)>>8)&0xFF)<<16)|	\
	((((x)>>16)&0xFF)<<8)|(((x)>>24)&0xFF)))

/* Type for 8-bit quantity. */
typedef int8_t SI8;
typedef uint8_t UI8;

/* Type for 16-bit quantity. */
typedef int16_t SI16;
typedef uint16_t UI16;

typedef int32_t SI24;
typedef uint32_t UI30;

/* Type for 32-bit quantity. */
typedef int32_t SI32;
typedef uint32_t UI32;

/* Type for 64-bit quantity. */
typedef int64_t SI64;
typedef uint64_t UI64;

/* Type for Fixed-Point numbers. */
typedef float Fixed8;
typedef float Fixed;

/* Type for Floating-point numbers. */
typedef uint16_t FLOAT16;
typedef float FLOAT;
typedef double DOUBLE;

/* Type for Encoded integers. */
typedef uint32_t EncodedU32;

/* Type for Bit values. */
typedef int SB;
typedef unsigned int UB;
typedef int FB;

/* Type for color */
typedef uint32_t ColorRGB;
typedef uint32_t ColorRGBA;

/* Type for strings. */

typedef UI8* String;
#define StringEnd '\0'

#define RECORDHEADER_TAG_TYPE(X) (X >> 6)
#define RECORDHEADER_TAG_LENGTH(X) (X & 0x3F)

#define RECORDHEADER_LENGTH_FULL 0x3F

enum TagCode
{
	End = 0,
	ShowFrame = 1,
	DefineShape = 2,
	PlaceObject = 4,
	RemoveObject = 5,
	DefineBits = 6,
	DefineButton = 7,
	JPEGTables = 8,
	SetBackgroundColor = 9,
	DefineFont = 10,
	DefineText = 11,
	DoAction = 12,
	DefineFontInfo = 13,
	DefineSound = 14,
	StartSound = 15,
	DefineButtonSound = 17,
	SoundStreamHead = 18,
	SoundStreamBlock = 19,
	DefineBitsLossless = 20,
	DefineBitsJPEG2 = 21,
	DefineShape2 = 22,
	DefineButtonCxform = 23,
	Protect = 24,
	PlaceObject2 = 26,
	RemoveObject2 = 28,
	DefineShape3 = 32,
	DefineText2 = 33,
	DefineButton2 = 34,
	DefineBitsJPEG3 = 35,
	DefineBitsLossless2 = 36,
	DefineEditText = 37,
	DefineSprite = 39,
	ProductInfo = 41,
	FrameLabel = 43,
	SoundStreamHead2 = 45,
	DefineMorphShape = 46,
	DefineFont2 = 48,
	ExportAssets = 56,
	ImportAssets = 57,
	EnableDebugger = 58,
	DoInitAction = 59,
	DefineVideoStream = 60,
	VideoFrame = 61,
	DefineFontInfo2 = 62,
	EnableDebugger2 = 64,
	ScriptLimits = 65,
	SetTabIndex = 66,
	FileAttributes = 69,
	PlaceObject3 = 70,
	ImportAssets2 = 71,
	DefineFontAlignZones = 73,
	CSMTextSettings = 74,
	DefineFont3 = 75,
	SymbolClass = 76,
	Metadata = 77,
	DefineScalingGrid = 78,
	DoABC = 82,
	DefineShape4 = 83,
	DefineMorphShape2 = 84,
	DefineSceneAndFrameLabelData = 86,
	DefineBinaryData = 87,
	DefineFontName = 88,
	StartSound2 = 89,
	DefineBitsJPEG4 = 90,
	DefineFont4 = 91
};

#endif // SWFTypes_h__
