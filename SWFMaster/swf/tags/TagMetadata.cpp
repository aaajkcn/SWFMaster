#include "../SWFTag.h"
#include "../SWFInputStream.h"
#include "../TagHandler.h"
#include <string>

void TagMetadata::setData( char* data, size_t len )
{
	SWFTag::setData(data, len);
}

void TagMetadata::visit( TagHandler* h )
{
	h->metadata(this);
}
