#ifndef TagDoABC_h__
#define TagDoABC_h__

#include "swf/SWFTag.h"
#include "swf/SWFTypes.h"
#include <string>

class ABCFile;

class TagDoABC : public SWFTag
{
public:
	std::string name;
	UI16 flags;
	ABCFile* abcFile;
protected:
	TagDoABC();
public:
	CREATE_FUNC(TagDoABC);
	~TagDoABC();
public:
	void setData(char* data, size_t len) override;
	void encode();
	void visit(TagHandler*) override;
};

#endif // TagDoABC_h__
