#include "SWFEncoder.h"
#include "SWF.h"
#include "utils/ImageHelper.h"
#include <QImage>
#include <assert.h>

int minBits(int number, int bits)
{
	int val = 1;
	for (int x = 1; val <= number && !(bits > 32); x <<= 1) 
	{
		val = val | x;
		bits++;
	}

	if (bits > 32)
	{
		//("minBits " + bits + " must not exceed 32");
		assert(false);
	}
	return bits;
}

void SWFEncoder::unknow( TagUnknow* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::doABC( TagDoABC* tag )
{
	tag->encode();
	this->writeTag(tag, _stream);
}

void SWFEncoder::metadata( TagMetadata* tag )
{
	_tagStream->writeString(tag->text);
	this->encodeTag(tag);
}

void SWFEncoder::frameLabel( TagFrameLabel* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::setBackgroundColor( TagSetBackgroundColor* tag )
{
	_tagStream->writeColorRGB(tag->color);
	this->encodeTag(tag);
}

void SWFEncoder::fileAttributes( TagFileAttributes* tag )
{
	UI32 flags = 0;
	if (tag->UseDirectBlit)
		flags |= (1 << 1);
	if (tag->UseGPU)
		flags |= (1 << 2);
	if (tag->HasMetadata)
		flags |= (1 << 3);
	if (tag->ActionScript3)
		flags |= (1 << 4);
	if (tag->UseNetwork)
		flags |= (1 << 7);
	_tagStream->writeUI32(flags);
	this->encodeTag(tag);
}

void SWFEncoder::end( TagEnd* tag )
{
	this->encodeTag(tag);
}

void SWFEncoder::scriptLimits( TagScriptLimits* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::symbolClass( TagSymbolClass* tag )
{
	bool hasTopLevelClass = tag->topLevelClass.empty() == false;
	UI16 num = tag->tags.size();
	if (hasTopLevelClass)
		num += 1;

	_tagStream->writeUI16(num);

	if (hasTopLevelClass)
	{
		_tagStream->writeUI16(0);
		_tagStream->writeString(tag->topLevelClass);
	}
	for (auto pair : tag->tags)
	{
		UI16 refid = this->getCharacterId(pair.second);
		_tagStream->writeUI16(refid);
		_tagStream->writeString(pair.first);
	}
	this->encodeTag(tag);
}

void SWFEncoder::importAssets2( TagImportAssets2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::protect( TagProtect* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::exportAssets( TagExportAssets* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::enableDebugger( TagEnableDebugger* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::enableDebugger2( TagEnableDebugger2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::setTabIndex( TagSetTabIndex* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineScalingGrid( TagDefineScalingGrid* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeRect(tag->Splitter);
	_doneList.push_back(tag);
	this->encodeTag(tag);
}

void SWFEncoder::defineSceneAndFrameLabelData( TagDefineSceneAndFrameLabelData* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineShape( TagDefineShape* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeRect(tag->ShapeBounds);
	this->encodeShapeWithStyle(tag->code, tag->Shapes);
	this->encodeTag(tag);
}

void SWFEncoder::defineShape2( TagDefineShape2* tag )
{
	this->defineShape(tag);
}

void SWFEncoder::defineShape3( TagDefineShape3* tag )
{
	this->defineShape(tag);
}

void SWFEncoder::defineShape4( TagDefineShape4* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeRect(tag->ShapeBounds);
	_tagStream->writeRect(tag->EdgeBounds);
	//Reserved
	_tagStream->writeUB(0, 5);
	_tagStream->writeUB(tag->UsesFillWindingRule, 1);
	_tagStream->writeUB(tag->UsesNonScalingStrokes, 1);
	_tagStream->writeUB(tag->UsesScalingStrokes, 1);
	
	encodeShapeWithStyle(tag->code, tag->Shapes);

	this->encodeTag(tag);
}

void SWFEncoder::defineButton( TagDefineButton* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineButton2( TagDefineButton2* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUB(0, 7);
	_tagStream->writeUB(tag->TrackAsMenu, 1);
	_tagStream->writeUI16(tag->ActionOffset);

	for (auto record : tag->Characters)
	{
		_tagStream->writeUB(record->ButtonReserved, 2);
		_tagStream->writeUB(record->ButtonHasBlendMode, 1);
		_tagStream->writeUB(record->ButtonHasFilterList, 1);
		_tagStream->writeUB(record->ButtonStateHitTest, 1);
		_tagStream->writeUB(record->ButtonStateDown, 1);
		_tagStream->writeUB(record->ButtonStateOver, 1);
		_tagStream->writeUB(record->ButtonStateUp, 1);

		_tagStream->writeUI16(this->getCharacterId(record->character));
		_tagStream->writeUI16(record->PlaceDepth);
		_tagStream->writeMatrix(record->PlaceMatrix);
		_tagStream->writeColorTransformWithAlpha(record->ColorTransform);

		if (record->ButtonHasFilterList)
		{
			record->FilterList.encode(_tagStream);
		}
		if (record->ButtonHasBlendMode)
		{
			_tagStream->writeUI8(record->BlendMode);
		}
	}
	//CharacterEndFlag
	_tagStream->writeUI8(0);
	//TODO: BUTTONCONDACTION
	_tagStream->writeByteArray(&tag->characterData);

	this->encodeTag(tag);
}

void SWFEncoder::defineButtonCxform( TagDefineButtonCxform* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineButtonSound( TagDefineButtonSound* tag )
{
	assert(false);
}

void SWFEncoder::defineSprite( TagDefineSprite* tag )
{
	//处理所有引用
	std::vector<SWFTagRef*> refs;
	std::vector<SWFTag*> allControlTags;
	tag->getAllControlTags(allControlTags);
	for (SWFTag* tag : allControlTags)
	{
		tag->getReferences(refs);
		for (SWFTagRef* tagRef : refs)
		{
			this->defineTag(tagRef->target);
		}
	}

	// char id & frame count
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUI16(tag->frameCount);

	//处理子Tag
	{
		SWFOutputStream* oldStream = _stream;
		SWFOutputStream* oldTagStream = _tagStream;
		_stream = new SWFOutputStream();
		_tagStream = new SWFOutputStream();
		for (SWFTag* tag : allControlTags)
		{
			tag->visit(this);
		}

		oldTagStream->writeByteArray(_stream->getByteArray());

		delete _stream;
		delete _tagStream;
		_stream = oldStream;
		_tagStream = oldTagStream;
	}
	//assert(false);
	this->encodeTag(tag);
}

void SWFEncoder::defineMorphShape( TagDefineMorphShape* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineMorphShape2( TagDefineMorphShape2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFont( TagDefineFont* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFont2( TagDefineFont2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFont3( TagDefineFont3* tag )
{
	UI16 charId = this->getCharacterId(tag);
	_tagStream->writeUI16(charId);
	_tagStream->writeByteArray(&tag->characterData);
	this->encodeTag(tag);
}

void SWFEncoder::defineFont4( TagDefineFont4* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFontInfo( TagDefineFontInfo* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFontInfo2( TagDefineFontInfo2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineFontAlignZones( TagDefineFontAlignZones* tag )
{
	UI16 charId = this->getCharacterId(tag->fontCharacter);
	_tagStream->writeUI16(charId);
	_tagStream->writeByteArray(&tag->bytes);
	this->encodeTag(tag);
}

void SWFEncoder::defineFontName( TagDefineFontName* tag )
{
	UI16 charId = this->getCharacterId(tag->fontCharacter);
	_tagStream->writeUI16(charId);
	_tagStream->writeString(tag->fontName);
	_tagStream->writeString(tag->fontCopyright);

	this->encodeTag(tag);
}

void SWFEncoder::defineText( TagDefineText* tag )
{
	UI16 charId = this->getCharacterId(tag);
	_tagStream->writeUI16(charId);
	_tagStream->writeRect(tag->TextBounds);
	_tagStream->writeMatrix(tag->TextMatrix);
	_tagStream->writeUI8(tag->GlyphBits);
	_tagStream->writeUI8(tag->AdvanceBits);

	for (TextRecord* rec : tag->TextRecords)
	{
		_tagStream->writeUI8(rec->flags);
		if (rec->StyleFlagsHasFont)
		{
			UI16 charId = this->getCharacterId(rec->fontCharacter);
			_tagStream->writeUI16(charId);
		}
		if (rec->StyleFlagsHasColor)
		{
			_tagStream->writeColorRGB(rec->TextColor);
		}
		if (rec->StyleFlagsHasXOffset)
		{
			_tagStream->writeSI16(rec->XOffset);
		}
		if (rec->StyleFlagsHasYOffset)
		{
			_tagStream->writeSI16(rec->YOffset);
		}
		if (rec->StyleFlagsHasFont)
		{
			_tagStream->writeUI16(rec->TextHeight);
		}

		_tagStream->writeUI8(rec->GlyphEntries.size());

		for (GlyphEntry* entry : rec->GlyphEntries)
		{
			_tagStream->writeUB(entry->GlyphIndex, tag->GlyphBits);
			_tagStream->writeSB(entry->GlyphAdvance, tag->AdvanceBits);
		}
		_tagStream->alignBits();
	}
	//end
	_tagStream->writeUI8(0);

	this->encodeTag(tag);
}

void SWFEncoder::defineText2( TagDefineText2* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::defineEditText( TagDefineEditText* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::cSMTextSettings( TagCSMTextSettings* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::showFrame( TagShowFrame* tag )
{
	this->encodeTag(tag);
}

void SWFEncoder::placeObject( TagPlaceObject* tag )
{
	//TODO: encode PlaceObject
	assert(false);
}

void SWFEncoder::placeObject2( TagPlaceObject2* tag )
{
	_tagStream->writeUI8(tag->flags1);
	_tagStream->writeUI16(tag->Depth);
	if (tag->hasCharacter())
	{
		UI16 cid = this->getCharacterId(tag->Character);
		_tagStream->writeUI16(cid);
	}
	if (tag->hasMatrix())
	{
		_tagStream->writeMatrix(tag->matrix);
	}
	if (tag->hasCxform())
	{
		_tagStream->writeColorTransformWithAlpha((CXFORMWITHALPHA*)tag->ColorTransform);
	}
	if (tag->hasRatio())
	{
		_tagStream->writeUI16(tag->Ratio);
	}
	if (tag->hasName())
	{
		_tagStream->writeString(tag->Name);
	}
	if (tag->hasClipDepth())
	{
		_tagStream->writeUI16(tag->ClipDepth);
	}
	if (tag->hasClipAction())
	{
		//TODO: encode ClipActions
		assert(false);
	}
	this->encodeTag(tag);
}

void SWFEncoder::placeObject3( TagPlaceObject3* tag )
{
	_tagStream->writeUI8(tag->flags1);
	_tagStream->writeUI8(tag->flags2);
	_tagStream->writeUI16(tag->Depth);
	if (tag->hasCharacter())
	{
		UI16 cid = this->getCharacterId(tag->Character);
		_tagStream->writeUI16(cid);
	}
	_tagStream->writeByteArray(&tag->remainData);

	this->encodeTag(tag);
}

void SWFEncoder::defineBitsJPEG2( TagDefineBitsJPEG2* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeByteArray(&tag->imageData);
	//TODO: image
	this->encodeTag(tag);
}

void SWFEncoder::defineBitsJPEG3( TagDefineBitsJPEG3* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUI32(tag->imageData.getLength());
	_tagStream->writeByteArray(&tag->imageData);
	_tagStream->writeByteArray(&tag->alphaData);
	this->encodeTag(tag);
}

void SWFEncoder::defineBinaryData( TagDefineBinaryData* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUI32(0);
	_tagStream->writeByteArray(&tag->binary);
	this->encodeTag(tag);
}

void SWFEncoder::productInfo( TagProductInfo* tag )
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::removeObject(TagRemoveObject* tag)
{
	this->writeTag(tag, _stream);
}

void SWFEncoder::removeObject2(TagRemoveObject2* tag)
{
	this->writeTag(tag, _stream);
}

int SWFEncoder::save( SWF* swf, const std::string& filePath )
{
	FILE* file;
	fopen_s(&file, filePath.c_str(), "wb");
	if (file == nullptr)
		return 0;

	_swf = swf;
	_filePath = filePath;
	_doneList.clear();
	_tagStream->reset();
	
	_stream = new SWFOutputStream();
	Rect frameSize;
	frameSize.Xmax = swf->width * 20;
	frameSize.Ymax = swf->height * 20;
	frameSize.NBits = 15;
	_stream->writeRect(frameSize);
	_stream->writeUI16(swf->frameRate << 8);
	_stream->writeUI16(swf->getFrameCount());
	
	if (swf->getFileAttributesTag())
		swf->getFileAttributesTag()->visit(this);
	//enableTelemetry
	if (swf->getMetadataTag())
		swf->getMetadataTag()->visit(this);
	if (swf->getBackgroundColorTag())
		swf->getBackgroundColorTag()->visit(this);
	if (swf->getScriptLimitsTag())
		swf->getScriptLimitsTag()->visit(this);
	if (swf->getProtectTag())
		swf->getProtectTag()->visit(this);
	if (swf->getEnableDebugger())
		swf->getEnableDebugger()->visit(this);
	if (swf->getEnableDebugger2())
		swf->getEnableDebugger2()->visit(this);
	//if (swf->defineSceneAndFrameLabelData)
	//	swf->defineSceneAndFrameLabelData->visit(this);

	TagShowFrame* showFrame = TagShowFrame::create();
	std::vector<SWFFrame*> allFrames;
	swf->getAllFrames(allFrames);
	for (SWFFrame* frame : allFrames)
	{
		if (frame->getFrameLable())
			frame->getFrameLable()->visit(this);
		//imports
		
		//define
		std::vector<DefineTag*> defines;
		frame->getAllDefineTags(defines);
		for (auto tag : defines)
		{
			this->defineTag(tag);
		}
		if (frame->getSymbolClass())
		{
			for (auto pair : frame->getSymbolClass()->tags)
			{
				this->defineTag(pair.second->target);
			}
		}
		//exports

		//doABC
		std::vector<TagDoABC*> allDoABCTags;
		frame->getAllDoABCTags(allDoABCTags);
		for (auto tag : allDoABCTags)
		{
			tag->visit(this);
		}
		//control
		std::vector<SWFTag*> allControlTags;
		frame->getAllControlTags(allControlTags);
		for (auto tag : allControlTags)
		{
			std::vector<SWFTagRef*> refs;
			tag->getReferences(refs);
			for (SWFTagRef* r : refs)
			{
				this->defineTag(r->target);
			}
			tag->visit(this);
		}
		//symbol class
		if (frame->getSymbolClass())
		{
			frame->getSymbolClass()->visit(this);
		}
		showFrame->visit(this);
	}
	TagEnd* end = TagEnd::create();
	end->visit(this);
	
	SWFOutputStream swfStream;
	swfStream.writeBytes("FWS", 3);
	swfStream.writeUI8(swf->version);
	swfStream.writeUI32(_stream->getPosition() + 8);
	swfStream.writeBytes(_stream->getData(), _stream->getPosition());

	fwrite(swfStream.getData(), 1, swfStream.getPosition(), file);
	fclose(file);

	return 1;
}

void SWFEncoder::writeTag( SWFTag* tag, SWFOutputStream* stream )
{
	UI32 tagCodeAndLength = tag->code << 6;
	size_t len = tag->length;
	if (len >= 63)
	{
		tagCodeAndLength = tagCodeAndLength | 63;
		_stream->writeUI16(tagCodeAndLength);
		_stream->writeUI32(len);
	}
	else
	{
		tagCodeAndLength = tagCodeAndLength | len;
		_stream->writeUI16(tagCodeAndLength);
	}
	_stream->writeBytes(tag->data, tag->length);
}

void SWFEncoder::defineTag(SWFTag* tag)
{
	auto itr = std::find(_doneList.begin(), _doneList.end(), tag);
	if (itr == _doneList.end())
	{
		_doneList.push_back(tag);
		//visit refs
		std::vector<SWFTagRef*> refs;
		tag->getReferences(refs);
		for (SWFTagRef* r : refs)
		{
			this->defineTag(r->target);
		}

		//visit
		tag->visit(this);
	}
}

SWFEncoder::SWFEncoder()
	: _stream(nullptr)
	, _swf(nullptr)
{
	_tagStream = new SWFOutputStream();
	_context = new SWFEncodeContext();
}

SWFEncoder::~SWFEncoder()
{
	if (_tagStream)
		delete _tagStream;
	if (_context)
		delete _context;
}

void SWFEncoder::encodeTag(SWFTag* tag)
{
	_tagStream->alignBits();

	ByteArray* bytes = _tagStream->getByteArray();
	UI32 tagCodeAndLength = tag->code << 6;
	size_t len = bytes->getLength();
	if (len >= 63)
	{
		tagCodeAndLength = tagCodeAndLength | 63;
		_stream->writeUI16(tagCodeAndLength);
		_stream->writeUI32(len);
	}
	else
	{
		tagCodeAndLength = tagCodeAndLength | len;
		_stream->writeUI16(tagCodeAndLength);
	}
	_stream->writeBytes(bytes->getData(), len);

	_tagStream->reset();
}

void SWFEncoder::encodeShapeWithStyle( UI8 tagCode, ShapeWithStyle* shape )
{
	_context->defineShapeCode = tagCode;

	shape->FillStyles->encode(_tagStream, _context);
	shape->LineStyles->encode(_tagStream, _context);
	shape->NumFillBits = minBits(shape->FillStyles->FillStyles.size(), 0);
	shape->NumLineBits = minBits(shape->LineStyles->LineStyles.size(), 0);

	_tagStream->writeUB(shape->NumFillBits, 4);
	_tagStream->writeUB(shape->NumLineBits, 4);
	
	for (ShapeRecord* record : shape->ShapeRecords)
	{
		record->encode(_tagStream, _context, shape);
	}
	EndShapeRecord end;
	end.encode(_tagStream, _context, shape);
	_tagStream->alignBits();
}

void SWFEncoder::defineBitsLossless( TagDefineBitsLossless* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUI8(tag->BitmapFormat);
	_tagStream->writeUI16(tag->BitmapWidth);
	_tagStream->writeUI16(tag->BitmapHeight);

	if (tag->BitmapFormat == 3)
	{
		_tagStream->writeUI8(tag->BitmapColorTableSize);
		//TODO:ALPHACOLORMAPDATA
		_tagStream->writeByteArray(&tag->ZlibBitmapData);
	}
	else if (tag->BitmapFormat == 4 || tag->BitmapFormat == 5)
	{
		//TODO:ALPHABITMAPDATA
		_tagStream->writeByteArray(&tag->ZlibBitmapData);
	}
	this->encodeTag(tag);
}

void SWFEncoder::defineBitsLossless2( TagDefineBitsLossless2* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeUI8(tag->BitmapFormat);
	_tagStream->writeUI16(tag->BitmapWidth);
	_tagStream->writeUI16(tag->BitmapHeight);

	if (tag->BitmapFormat == 3)
	{
		_tagStream->writeUI8(tag->BitmapColorTableSize);
		//TODO:ALPHACOLORMAPDATA
		_tagStream->writeByteArray(&tag->ZlibBitmapData);
	}
	else if (tag->BitmapFormat == 4 || tag->BitmapFormat == 5)
	{
		//TODO:ALPHABITMAPDATA
		_tagStream->writeByteArray(&tag->ZlibBitmapData);
	}
	this->encodeTag(tag);
}

UI16 SWFEncoder::getCharacterId(SWFTag* tag)
{
	return _context->getCharacterId(tag);
}

UI16 SWFEncoder::getCharacterId( SWFTagRef* tagRef )
{
	return _context->getCharacterId(tagRef);
}

void SWFEncoder::defineSound( TagDefineSound* tag )
{
	_tagStream->writeUI16(this->getCharacterId(tag));
	_tagStream->writeByteArray(&tag->soundData);
	this->encodeTag(tag);
}

UI16 SWFEncodeContext::getCharacterId(SWFTag* tag)
{
	if (tag == nullptr)
		return 0xffff;
	assert(tag);
	auto pair = _characterIdMap.find(tag);
	if (pair == _characterIdMap.end())
	{
		UI16 id = _characterIdCount++;
		_characterIdMap[tag] = id;
		return id;
	}

	return pair->second;
}

UI16 SWFEncodeContext::getCharacterId( SWFTagRef* r )
{
	if (r == nullptr)
		return getCharacterId((SWFTag*)nullptr);
	return getCharacterId(r->target);
}
