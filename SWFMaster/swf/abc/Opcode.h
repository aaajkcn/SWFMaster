#ifndef Opcode_h__
#define Opcode_h__

#include "swf/SWFTypes.h"
#include <vector>
#include <string>
#include <map>

class CpoolInfo;
class SWFInputStream;

enum class OpcodeArgType
{
	A_Namespace,
	A_NamesapceSet,
	A_Multiname,
	A_String,
	A_int,
	A_uint,
	A_double,
	A_U30,
	A_S24,
	A_UByte
};

enum class Opcode
{
	OP_add				= 0xA0,//��, value1, value2 => ��, value3
	OP_add_i			= 0xC5,//��, value1, value2 => ��, value3
	OP_astype			= 0x86,//
	OP_astypelate		= 0x87,
	OP_applytype		= 0x53,
	OP_bitand			= 0xa8,
	OP_bitnot			= 0x97,
	OP_bitor			= 0xa9,
	OP_bitxor			= 0xaa,
	OP_call				= 0x41,
	OP_callmethod		= 0x43,
	OP_callproperty		= 0x46,
	OP_callproplex		= 0x4c,
	OP_callpropvoid		= 0x4f,
	OP_callstatic		= 0x44,
	OP_callsuper		= 0x45,
	OP_callsupervoid	= 0x4e,
	OP_checkfilter		= 0x78,
	OP_coerce			= 0x80,
	OP_coerce_a			= 0x82,
	OP_coerce_s			= 0x85,
	OP_construct		= 0x42,
	OP_constructprop	= 0x4a,
	OP_constructsuper	= 0x49,
	OP_convert_b		= 0x76,
	OP_convert_i		= 0x73,
	OP_convert_d		= 0x75,
	OP_convert_o		= 0x77,
	OP_convert_u		= 0x74,
	OP_convert_s		= 0x70,
	OP_debug			= 0xef,
	OP_debugfile		= 0xf1,
	OP_debugline		= 0xf0,
	OP_declocal			= 0x94,
	OP_declocal_i		= 0xc3,
	OP_decrement		= 0x93,
	OP_decrement_i		= 0xc1,
	OP_deleteproperty	= 0x6a,
	OP_divide			= 0xa3,
	OP_dup				= 0x2a,
	OP_dxns				= 0x06,
	OP_dxnslate			= 0x07,
	OP_equals			= 0xab,
	OP_esc_xattr		= 0x72,
	OP_esc_xelem		= 0x71,
	OP_findproperty		= 0x5e,
	OP_findpropstrict	= 0x5d,
	OP_getdescendants	= 0x59,
	OP_getglobalscope	= 0x64,
	OP_getglobalslot	= 0x6e,
	OP_getlex			= 0x60,
	OP_getlocal			= 0x62,
	OP_getlocal_0		= 0xd0,
	OP_getlocal_1		= 0xd1,
	OP_getlocal_2		= 0xd2,
	OP_getlocal_3		= 0xd3,
	OP_getproperty		= 0x66,
	OP_getscopeobject	= 0x65,
	OP_getslot			= 0x6c,
	OP_getsuper			= 0x04,
	OP_greaterequals	= 0xb0,
	OP_greaterthan		= 0xaf,
	OP_hasnext			= 0x1f,
	OP_hasnext2			= 0x32,
	OP_ifeq				= 0x13,
	OP_iffalse			= 0x12,
	OP_ifge				= 0x18,
	OP_ifgt				= 0x17,
	OP_ifle				= 0x16,
	OP_iflt				= 0x15,
	OP_ifnge			= 0x0f,
	OP_ifngt			= 0x0e,
	OP_ifnle			= 0x0d,
	OP_ifnlt			= 0x0c,
	OP_ifne				= 0x14,
	OP_ifstricteq		= 0x19,
	OP_ifstrictne		= 0x1a,
	OP_iftrue			= 0x11,
	OP_in				= 0xb4,
	OP_inclocal			= 0x92,
	OP_inclocal_i		= 0xc2,
	OP_increment		= 0x91,
	OP_increment_i		= 0xc0,
	OP_initproperty		= 0x68,
	OP_instanceof		= 0xb1,
	OP_istype			= 0xb2,
	OP_istypelate		= 0xb3,
	OP_jump				= 0x10,
	OP_kill				= 0x08,
	OP_label			= 0x09,
	OP_lessequals		= 0xae,
	OP_lessthan			= 0xad,
	OP_lookupswitch		= 0x1b,
	OP_lshift			= 0xa5,
	OP_modulo			= 0xa4,
	OP_multiply			= 0xa2,
	OP_multiply_i		= 0xc7,
	OP_negate			= 0x90,
	OP_negate_i			= 0xc4,
	OP_newactivation	= 0x57,
	OP_newarray			= 0x56,
	OP_newcatch			= 0x5a,
	OP_newclass			= 0x58,
	OP_newfunction		= 0x40,
	OP_newobject		= 0x55,
	OP_nextname			= 0x1e,
	OP_nextvalue		= 0x23,
	OP_nop				= 0x02,
	OP_not				= 0x96,
	OP_pop				= 0x29,
	OP_popscope			= 0x1d,
	OP_pushbyte			= 0x24,
	OP_pushdouble		= 0x2f,
	OP_pushfalse		= 0x27,
	OP_pushint			= 0x2d,
	OP_pushnamespace	= 0x31,
	OP_pushnan			= 0x28,
	OP_pushnull			= 0x20,
	OP_pushscope		= 0x30,
	OP_pushshort		= 0x25,
	OP_pushstring		= 0x2c,
	OP_pushtrue			= 0x26,
	OP_pushuint			= 0x2e,
	OP_pushundefined	= 0x21,
	OP_pushwith			= 0x1c,
	OP_returnvalue		= 0x48,
	OP_returnvoid		= 0x47,
	OP_rshift			= 0xa6,
	OP_setlocal			= 0x63,
	OP_setlocal_0		= 0xd4,
	OP_setlocal_1		= 0xd5,
	OP_setlocal_2		= 0xd6,
	OP_setlocal_3		= 0xd7,
	OP_setglobalslot	= 0x6f,
	OP_setproperty		= 0x61,
	OP_setslot			= 0x6d,
	OP_setsuper			= 0x05,
	OP_strictequals		= 0xac,
	OP_subtract			= 0xa1,
	OP_subtract_i		= 0xc6,
	OP_swap				= 0x2b,
	OP_throw			= 0x03,
	OP_typeof			= 0x95,
	OP_urshift			= 0xa7
};


class OpcodeOperation
{
public:
	Opcode code;
	std::vector<OpcodeArgType> arguments;
	std::string name;
public:
	OpcodeOperation(Opcode code, std::string name, int count, ...);
	inline size_t argLen() const { return arguments.size(); }
private:
	static std::map<Opcode, OpcodeOperation*> opMap;
public:
	static OpcodeOperation* getOperation(Opcode code);
public:
	static OpcodeOperation* op_add;
	static OpcodeOperation* op_add_i;
	static OpcodeOperation* op_astype;
	static OpcodeOperation* op_astypelate;
	static OpcodeOperation* op_applytype;
	static OpcodeOperation* op_bitand;
	static OpcodeOperation* op_bitnot;
	static OpcodeOperation* op_bitor;
	static OpcodeOperation* op_bitxor;
	static OpcodeOperation* op_call;
	static OpcodeOperation* op_callmethod;
	static OpcodeOperation* op_callproperty;
	static OpcodeOperation* op_callproplex;
	static OpcodeOperation* op_callpropvoid;
	static OpcodeOperation* op_callstatic;
	static OpcodeOperation* op_callsuper;
	static OpcodeOperation* op_callsupervoid;
	static OpcodeOperation* op_checkfilter;
	static OpcodeOperation* op_coerce;
	static OpcodeOperation* op_coerce_a;
	static OpcodeOperation* op_coerce_s;
	static OpcodeOperation* op_construct;
	static OpcodeOperation* op_constructprop;
	static OpcodeOperation* op_constructsuper;
	static OpcodeOperation* op_convert_b;
	static OpcodeOperation* op_convert_i;
	static OpcodeOperation* op_convert_d;
	static OpcodeOperation* op_convert_o;
	static OpcodeOperation* op_convert_u;
	static OpcodeOperation* op_convert_s;
	static OpcodeOperation* op_debug;
	static OpcodeOperation* op_debugfile;
	static OpcodeOperation* op_debugline;
	static OpcodeOperation* op_declocal;
	static OpcodeOperation* op_declocal_i;
	static OpcodeOperation* op_decrement;
	static OpcodeOperation* op_decrement_i;
	static OpcodeOperation* op_deleteproperty;
	static OpcodeOperation* op_divide;
	static OpcodeOperation* op_dup;
	static OpcodeOperation* op_dxns;
	static OpcodeOperation* op_dxnslate;
	static OpcodeOperation* op_equals;
	static OpcodeOperation* op_esc_xattr;
	static OpcodeOperation* op_esc_xelem;
	static OpcodeOperation* op_findproperty;
	static OpcodeOperation* op_findpropstrict;
	static OpcodeOperation* op_getdescendants;
	static OpcodeOperation* op_getglobalscope;
	static OpcodeOperation* op_getglobalslot;
	static OpcodeOperation* op_getlex;
	static OpcodeOperation* op_getlocal;
	static OpcodeOperation* op_getlocal_0;
	static OpcodeOperation* op_getlocal_1;
	static OpcodeOperation* op_getlocal_2;
	static OpcodeOperation* op_getlocal_3;
	static OpcodeOperation* op_getproperty;
	static OpcodeOperation* op_getscopeobject;
	static OpcodeOperation* op_getslot;
	static OpcodeOperation* op_getsuper;
	static OpcodeOperation* op_greaterequals;
	static OpcodeOperation* op_greaterthan;
	static OpcodeOperation* op_hasnext;
	static OpcodeOperation* op_hasnext2;
	static OpcodeOperation* op_ifeq;
	static OpcodeOperation* op_iffalse;
	static OpcodeOperation* op_ifge;
	static OpcodeOperation* op_ifgt;
	static OpcodeOperation* op_ifle;
	static OpcodeOperation* op_iflt;
	static OpcodeOperation* op_ifnge;
	static OpcodeOperation* op_ifngt;
	static OpcodeOperation* op_ifnle;
	static OpcodeOperation* op_ifnlt;
	static OpcodeOperation* op_ifne;
	static OpcodeOperation* op_ifstricteq;
	static OpcodeOperation* op_ifstrictne;
	static OpcodeOperation* op_iftrue;
	static OpcodeOperation* op_in;
	static OpcodeOperation* op_inclocal;
	static OpcodeOperation* op_inclocal_i;
	static OpcodeOperation* op_increment;
	static OpcodeOperation* op_increment_i;
	static OpcodeOperation* op_initproperty;
	static OpcodeOperation* op_instanceof;
	static OpcodeOperation* op_istype;
	static OpcodeOperation* op_istypelate;
	static OpcodeOperation* op_jump;
	static OpcodeOperation* op_kill;
	static OpcodeOperation* op_label;
	static OpcodeOperation* op_lessequals;
	static OpcodeOperation* op_lessthan;
	static OpcodeOperation* op_lookupswitch;
	static OpcodeOperation* op_lshift;
	static OpcodeOperation* op_modulo;
	static OpcodeOperation* op_multiply;
	static OpcodeOperation* op_multiply_i;
	static OpcodeOperation* op_negate;
	static OpcodeOperation* op_negate_i;
	static OpcodeOperation* op_newactivation;
	static OpcodeOperation* op_newarray;
	static OpcodeOperation* op_newcatch;
	static OpcodeOperation* op_newclass;
	static OpcodeOperation* op_newfunction;
	static OpcodeOperation* op_newobject;
	static OpcodeOperation* op_nextname;
	static OpcodeOperation* op_nextvalue;
	static OpcodeOperation* op_nop;
	static OpcodeOperation* op_not;
	static OpcodeOperation* op_pop;
	static OpcodeOperation* op_popscope;
	static OpcodeOperation* op_pushbyte;
	static OpcodeOperation* op_pushdouble;
	static OpcodeOperation* op_pushfalse;
	static OpcodeOperation* op_pushint;
	static OpcodeOperation* op_pushnamespace;
	static OpcodeOperation* op_pushnan;
	static OpcodeOperation* op_pushnull;
	static OpcodeOperation* op_pushscope;
	static OpcodeOperation* op_pushshort;
	static OpcodeOperation* op_pushstring;
	static OpcodeOperation* op_pushtrue;
	static OpcodeOperation* op_pushuint;
	static OpcodeOperation* op_pushundefined;
	static OpcodeOperation* op_pushwith;
	static OpcodeOperation* op_returnvalue;
	static OpcodeOperation* op_returnvoid;
	static OpcodeOperation* op_rshift;
	static OpcodeOperation* op_setlocal;
	static OpcodeOperation* op_setlocal_0;
	static OpcodeOperation* op_setlocal_1;
	static OpcodeOperation* op_setlocal_2;
	static OpcodeOperation* op_setlocal_3;
	static OpcodeOperation* op_setglobalslot;
	static OpcodeOperation* op_setproperty;
	static OpcodeOperation* op_setslot;
	static OpcodeOperation* op_setsuper;
	static OpcodeOperation* op_strictequals;
	static OpcodeOperation* op_subtract;
	static OpcodeOperation* op_subtract_i;
	static OpcodeOperation* op_swap;
	static OpcodeOperation* op_throw;
	static OpcodeOperation* op_typeof;
	static OpcodeOperation* op_urshift;
};

class OpcodeInstruction
{
public:
	OpcodeOperation* operation;
	Opcode code;
	char* data;
	size_t dataLen;
	std::string paramString;
public:
	OpcodeInstruction(OpcodeOperation* operation, Opcode code, char* data, size_t dataLen);
	~OpcodeInstruction();
};

class OpcodeAnalyzer
{
public:
	void analyze(CpoolInfo* cpool, SWFInputStream* input, std::vector<OpcodeInstruction*>& instructions);
};
#endif // Opcode_h__
