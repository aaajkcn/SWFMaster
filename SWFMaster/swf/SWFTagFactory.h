#ifndef SWFTagFactory_h__
#define SWFTagFactory_h__

#include "SWFTag.h"
#include "SWFTypes.h"
#include <functional>
#include <map>

class SWFTagFactory
{
public:
	SWFTagFactory();

	SWFTag* create(UI16 tagCode);

private:
	typedef std::function<SWFTag*()> TagCreator;

	template<class T>
	void registerTag(UI16 tagCode)
	{
		TagCreator creator = [](){
			return T::create();
		};
		_creatorMap[tagCode] = creator;
	}
private:
	std::map<UI16, TagCreator> _creatorMap;
};
#endif // SWFTagFactory_h__
