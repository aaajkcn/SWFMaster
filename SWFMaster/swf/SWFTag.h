#ifndef SWFTag_h__
#define SWFTag_h__

#include "SWFTypes.h"
#include "SWFStructs.h"
#include <string>
#include <vector>
#include <map>
#include "ByteArray.h"
#include "SWFInputStream.h"
#include "SWFOutputStream.h"
#include "Object.h"

class TagHandler;
class DefineTag;
class SWFTagRef;

#define HAS_CLIP_ACTION  1 << 7
#define HAS_CLIP_DEPTH  1 << 6
#define HAS_NAME  1 << 5
#define HAS_RATIO  1 << 4
#define HAS_CXFORM  1 << 3
#define HAS_MATRIX  1 << 2
#define HAS_CHARACTER  1 << 1
#define HAS_MOVE  1 << 0

#define HAS_IMAGE  1 << 4
#define HAS_CLASS_NAME  1 << 3
#define HAS_CACHE_AS_BITMAP  1 << 2
#define HAS_BLEND_MODE  1 << 1
#define HAS_FILTER_LIST  1 << 0

class SWFTag
	: public Object
{
public:
	int code;
	size_t length;
	char* data;
protected:
	std::string _tagName;
	std::vector<SWFTagRef*> _references;
public:
	SWFTag();
	SWFTag(int tagCode);
	~SWFTag();
	void putReference(SWFTagRef* tag);
	void removeReference(SWFTagRef* tag);
	void removeReference(SWFTag* tag);
	void updateReference(SWFTag* oldTag, SWFTag* newTag);
	SWFTagRef* getReference(SWFTag*) const;

	virtual void setData(char* data, size_t len);
	virtual void read(SWFInputStream* stream);
	virtual void getReferences(std::vector<SWFTagRef*>&);
	virtual void write(SWFOutputStream* stream);
	virtual void visit(TagHandler*);
	virtual std::string getTagName() const;
};

class DefineTag
	: public SWFTag
{
public:
	UI16 characterID;
	ByteArray characterData;
};

class DisplayTag
	: public DefineTag
{
public:
	virtual Rect getBounds() const = 0;
};

class SWFTagRef
{
private:
	SWFTagRef() : target(nullptr) {};
public:
	static SWFTagRef* create(DefineTag* tag)
	{
		assert(tag);
		SWFTagRef* r = new SWFTagRef();
		r->target = tag;
		return r;
	}
public:
	DefineTag* target;
};

#pragma region Control Tags

class TagMetadata : public SWFTag
{
private:
	TagMetadata(){};
public:
	CREATE_FUNC(TagMetadata);
	std::string text;
public:
	void setData(char* data, size_t len) override;
	void visit(TagHandler*) override;
};

class TagProductInfo
	: public SWFTag
{
private:
	TagProductInfo(){};
public:
	CREATE_FUNC(TagProductInfo);
	void visit(TagHandler* h);
};

class TagSetBackgroundColor
	: public SWFTag
{
public:
	ColorRGB color;
private:
	TagSetBackgroundColor();
public:
	CREATE_FUNC(TagSetBackgroundColor);

	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagFrameLabel
	: public SWFTag
{
public:
	std::string name;
private:
	TagFrameLabel();
public:
	CREATE_FUNC(TagFrameLabel);
	void visit(TagHandler* h) override;
};

class TagFileAttributes
	: public SWFTag
{
public:
	bool UseDirectBlit;
	bool UseGPU;
	bool HasMetadata;
	bool ActionScript3;
	bool UseNetwork;
private:
	TagFileAttributes();
public:
	CREATE_FUNC(TagFileAttributes);

	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnd
	: public SWFTag
{
private:
	TagEnd();
public:
	CREATE_FUNC(TagEnd);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagScriptLimits
	: public SWFTag
{
private:
	TagScriptLimits();
public:
	CREATE_FUNC(TagScriptLimits);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagSymbolClass
	: public SWFTag
{
public:
	std::string topLevelClass;
	std::map<std::string, SWFTagRef*> tags;
private:
	TagSymbolClass();
public:
	CREATE_FUNC(TagSymbolClass);
	void visit(TagHandler* h) override;
};

class TagImportAssets2
	: public SWFTag
{
private:
	TagImportAssets2();
public:
	CREATE_FUNC(TagImportAssets2);

	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagProtect
	: public SWFTag
{
private:
	TagProtect();
public:
	CREATE_FUNC(TagProtect);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagExportAssets
	: public SWFTag
{
private:
	TagExportAssets();
public:
	CREATE_FUNC(TagExportAssets);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnableDebugger
	: public SWFTag
{
private:
	TagEnableDebugger();
public:
	CREATE_FUNC(TagEnableDebugger);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagEnableDebugger2
	: public SWFTag
{
private:
	TagEnableDebugger2();
public:
	CREATE_FUNC(TagEnableDebugger2);

	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagSetTabIndex
	: public SWFTag
{
private:
	TagSetTabIndex();
public:
	CREATE_FUNC(TagSetTabIndex);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineScalingGrid
	: public DefineTag
{
public:
	Rect Splitter;
private:
	TagDefineScalingGrid();
public:
	CREATE_FUNC(TagDefineScalingGrid);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineSceneAndFrameLabelData
	: public DefineTag
{
private:
	TagDefineSceneAndFrameLabelData();
public:
	CREATE_FUNC(TagDefineSceneAndFrameLabelData);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Control Tags

#pragma region Shapes
class TagDefineShape
	: public DisplayTag
{
public:
	Rect ShapeBounds;
	ShapeWithStyle* Shapes;
protected:
	TagDefineShape();
public:
	CREATE_FUNC(TagDefineShape);
	void visit(TagHandler* h) override;
	Rect getBounds() const override { return ShapeBounds; }
};

class TagDefineShape2
	: public TagDefineShape
{
private:
	TagDefineShape2();
public:
	CREATE_FUNC(TagDefineShape2);
	void visit(TagHandler* h) override;
};

class TagDefineShape3
	: public TagDefineShape
{
private:
	TagDefineShape3();
public:
	CREATE_FUNC(TagDefineShape3);
	void visit(TagHandler* h) override;
};

class TagDefineShape4
	: public TagDefineShape
{
public:
	Rect EdgeBounds;
	// UB[1]  If 1, use fill winding rule. Minimum
	// file format version is SWF 10
	UB UsesFillWindingRule;
	// UB[1]  If 1, the shape contains at least one
	// non-scaling stroke.
	UB UsesNonScalingStrokes;
	// UB[1]  If 1, the shape contains at least one
	// scaling stroke.
	UB UsesScalingStrokes;
private:
	TagDefineShape4();
public:
	CREATE_FUNC(TagDefineShape4);
	void visit(TagHandler* h) override;
};
#pragma endregion Shapes

#pragma region Sound
class TagDefineSound
	: public DefineTag
{
public:
	ByteArray soundData;
protected:
	TagDefineSound(){};
public:
	CREATE_FUNC(TagDefineSound);
	void visit(TagHandler* h) override;
};
#pragma endregion Sound

#pragma region Buttons

class TagDefineButton
	: public DisplayTag
{
public:
	std::vector<ButtonRecord*> Characters;
	std::vector<ActionRecord*> Actions;
protected:
	TagDefineButton();
public:
	CREATE_FUNC(TagDefineButton);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
	Rect getBounds() const override;
};

class TagDefineButton2
	: public DisplayTag
{
public:
	UB TrackAsMenu;
	UI16 ActionOffset;
	std::vector<ButtonRecord*> Characters;
private:
	TagDefineButton2();
public:
	CREATE_FUNC(TagDefineButton2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
	Rect getBounds() const override;
};

class TagDefineButtonCxform
	: public DefineTag
{
public:
	// Button ID for this information
	UI16 ButtonId;
	// Character color transform
	CXFORM* ButtonColorTransform;
protected:
	TagDefineButtonCxform();
public:
	CREATE_FUNC(TagDefineButtonCxform);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagDefineButtonSound
	: public DefineTag
{
protected:
	TagDefineButtonSound();
public:
	CREATE_FUNC(TagDefineButtonSound);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

#pragma endregion Buttons

#pragma region Sprites And Movie Clips
class TagDefineSprite
	: public DisplayTag
{
public:
	UI16 frameCount;
protected:
	TagDefineSprite();
	std::vector<SWFTag*> tags;
public:
	CREATE_FUNC(TagDefineSprite);
	~TagDefineSprite();

	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);

	void addControlTag(SWFTag* tag);
	void getAllControlTags(std::vector<SWFTag*>& output) const;

	virtual Rect getBounds() const override;
};
#pragma endregion Sprites And Movie Clips

#pragma region Shape Morphing
class TagDefineMorphShape
	: public DefineTag
{
protected:
	TagDefineMorphShape();
public:
	CREATE_FUNC(TagDefineMorphShape);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineMorphShape2
	: public DefineTag
{
protected:
	TagDefineMorphShape2();
public:
	CREATE_FUNC(TagDefineMorphShape2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Shape Morphing

#pragma region Fonts and Text
class TagDefineFont
	: public DefineTag
{
protected:
	TagDefineFont();
public:
	CREATE_FUNC(TagDefineFont);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont2
	: public DefineTag
{
protected:
	TagDefineFont2();
public:
	CREATE_FUNC(TagDefineFont2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont3
	: public DefineTag
{
public:
	// UB[1]
	// Has font metrics/layout information.
	UB FontFlagsHasLayout;
	// UB[1]
	// ShiftJIS encoding.
	UB FontFlagsShiftJIS;
	// UB[1]
	// SWF 7 or later: Font is small. Character glyphs
	// are aligned on pixel boundaries for dynamic and input text.
	UB FontFlagsSmallText;
	// UB[1]
	// ANSI encoding.
	UB FontFlagsANSI;
	// UB[1]
	// If 1, uses 32 bit offsets.
	UB FontFlagsWideOffsets;
	// UB[1]
	// Must be 1.
	UB FontFlagsWideCodes;
	// UB[1]
	// Italic Font
	UB FontFlagsItalic;
	// UB[1]
	// Bold Font.
	UB FontFlagsBold;
	// SWF 5 or earlier: always 0; SWF 6 or later: language code(UI8)
	UI8 LanguageCode;
	// UI8
	// Length of name.
	UI8 FontNameLen;
	// UI8[FontNameLen]
	// Name of font (see DefineFontInfo).
	std::string FontName;
	// UI16
	// Count of glyphs in font. May be zero for device fonts.
	UI16 NumGlyphs;
	// If FontFlagsWideOffsets, UI32[NumGlyphs] Otherwise UI16[NumGlyphs]
	// Array of shape offsets
	std::vector<UI32> OffsetTable;
	// If FontFlagsWideOffsets, UI32 Otherwise UI16
	// Byte count from start of OffsetTable to start of CodeTable.
	UI32 CodeTableOffset;
	// Array of shapes
	std::vector<Shape*> GlyphShapeTable;
	// UI16[NumGlyphs]
	// Sorted in ascending order. Always UCS-2 in SWF 6 or later.
	std::vector<UI16> CodeTable;
	// If FontFlagsHasLayout, UI16
	// Font ascender height.
	UI16 FontAscent;
	// If FontFlagsHasLayout, UI16
	// Font descender height.
	UI16 FontDescent;
	// If FontFlagsHasLayout, SI16
	// Font leading height (see following).
	SI16 FontLeading;
	// If FontFlagsHasLayout, SI16[NumGlyphs]
	// Advance value to be used for each glyph in dynamic glyph text.
	std::vector<SI16> FontAdvanceTable;
	// If FontFlagsHasLayout, RECT[NumGlyphs]
	// Not used in Flash Player through version 7 (always set to 0 to save space).
	std::vector<Rect> FontBoundsTable;
	// If FontFlagsHasLayout, KERNINGRECORD [KerningCount]
	// Not used in Flash Player through version 7 (omit with KerningCount of 0).
	std::vector<KerningRecord*> FontKerningTable;
protected:
	TagDefineFont3();
public:
	CREATE_FUNC(TagDefineFont3);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFont4
	: public DefineTag
{
protected:
	TagDefineFont4();
public:
	CREATE_FUNC(TagDefineFont4);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontInfo
	: public DefineTag
{
protected:
	TagDefineFontInfo();
public:
	CREATE_FUNC(TagDefineFontInfo);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontInfo2
	: public DefineTag
{
protected:
	TagDefineFontInfo2();
public:
	CREATE_FUNC(TagDefineFontInfo2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontAlignZones
	: public SWFTag
{
public:
	SWFTagRef* fontCharacter;
	ByteArray bytes;
protected:
	TagDefineFontAlignZones();
public:
	CREATE_FUNC(TagDefineFontAlignZones);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineFontName
	: public SWFTag
{
public:
	SWFTagRef* fontCharacter;
	std::string fontName;
	std::string fontCopyright;
protected:
	TagDefineFontName();
public:
	CREATE_FUNC(TagDefineFontName);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineText
	: public DisplayTag
{
public:
	// Bounds of the text.
	Rect TextBounds;
	// MATRIX
	// Transformation matrix for the text.
	Matrix TextMatrix;
	// UI8
	// Bits in each glyph index.
	UI8 GlyphBits;
	// UI8
	// Bits in each advance value.
	UI8 AdvanceBits;
	// TEXTRECORD[zero or more]  Text records.
	std::vector<TextRecord*> TextRecords;
	// UI8  Must be 0.
	UI8	EndOfRecordsFlag;
protected:
	TagDefineText();
public:
	CREATE_FUNC(TagDefineText);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
	Rect getBounds() const override { return TextBounds; }
};

class TagDefineText2
	: public DefineTag
{
protected:
	TagDefineText2();
public:
	CREATE_FUNC(TagDefineText2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagDefineEditText
	: public DefineTag
{
public:
	// Rectangle that completely encloses the text field.
	Rect bounds;
	// UB[1]
	// 0 = text field has no default text. 1 = text field initially displays the string specified by InitialText.
	UB HasText;
	// UB[1]
	// 0 = text will not wrap and will scroll sideways. 1 = text will wrap automatically when the end of line is reached.
	UB WordWrap;
	// UB[1]
	// 0 = text field is one line only. 1 = text field is multi-line and scrollable.
	UB Multiline;
	// UB[1]
	// 0 = characters are displayed as typed. 1 = all characters are displayed as an asterisk.
	UB Password;
	// UB[1]
	// 0 = text editing is enabled. 1 = text editing is disabled.
	UB ReadOnly;
	// UB[1]
	// 0 = use default color. 1 = use specified color (TextColor).
	UB HasTextColor;
	// UB[1]
	// 0 = length of text is unlimited. 1 = maximum length of string is specified by MaxLength.
	UB HasMaxLength;
	// UB[1]
	// 0 = use default font. 1 = use specified font (FontID) and height (FontHeight). (Can��t be true if HasFontClass is true).
	UB HasFont;
	// UB[1]
	// 0 = no fontClass, 1 = fontClass and Height specified for this text. (can't be true if HasFont is true). Supported in Flash Player 9.0.45.0 and later.
	UB HasFontClass;
	// UB[1]
	// 0 = fixed size. 1 = sizes to content (SWF 6 or later only).
	UB AutoSize;
	// UB[1]
	// Layout information provided.
	UB HasLayout;
	// UB[1]
	// Enables or disables interactive text selection.
	UB NoSelect;
	// UB[1]
	// Causes a border to be drawn around the text field.
	UB Border;
	// UB[1]
	// 0 = Authored as dynamic text; 1 = Authored as static text
	UB WasStatic;
	// UB[1]
	// 0 = plaintext content. 1 = HTML content (see following).
	UB HTML;
	// UB[1]
	// 0 = use device font. 1 = use glyph font.
	UB UseOutlines;
	// If HasFont
	// ID of font to use.
	UI16 FontId;
	SWFTagRef* font;
	// If HasFontClass, STRING 
	// Class name of font to be loaded from another SWF and used for this text.
	std::string FontClass;
	// If HasFont, UI16
	// Height of font in twips.
	UI16 FontHeight;
	// If HasTextColor, RGBA
	// Color of text.
	ColorRGBA TextColor;
	// If HasMaxLength, UI16
	// Text is restricted to this length.
	UI16 MaxLength;
	// If HasLayout, UI8
	// 0 = Left; 1 = Right; 2 = Center; 3 = Justify
	UI8 Align;
	// If HasLayout, UI16
	// Left margin in twips.
	UI16 LeftMargin;
	// If HasLayout, UI16
	// Right margin in twips.
	UI16 RightMargin;
	// If HasLayout, UI16
	// Indent in twips.
	UI16 Indent;
	// If HasLayout, SI16
	// Leading in twips (vertical distance between bottom of 
	// descender of one line and top of ascender of the next).
	SI16 Leading;
	// Name of the variable where the contents of the text field
	// are stored. May be qualified with dot syntax or slash syntax
	// for non-global variables.
	std::string VariableName;
	// If HasText STRING 
	// Text that is initially displayed.
	std::string InitialText;
protected:
	TagDefineEditText();
public:
	CREATE_FUNC(TagDefineEditText);
	void visit(TagHandler* h) override;
};

class TagCSMTextSettings
	: public SWFTag
{
protected:
	TagCSMTextSettings();
public:
	CREATE_FUNC(TagCSMTextSettings);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};
#pragma endregion Fonts and Text

#pragma region The Display List
class TagShowFrame
	: public SWFTag
{
protected:
	TagShowFrame();
public:
	CREATE_FUNC(TagShowFrame);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len);
};

class TagPlaceObject
	: public SWFTag
{
public:
	UI16 Depth;
	Matrix matrix;
	CXFORM* ColorTransform;
protected:
	TagPlaceObject()
		: Depth(0)
		, ColorTransform(nullptr)
	{

	}
public:
	CREATE_FUNC(TagPlaceObject);
	void visit(TagHandler* h) override;
};

class TagPlaceObject2
	: public TagPlaceObject
{
public:
	UI8 flags1;
	SWFTagRef* Character;
	UI16 Ratio;
	std::string Name;
	UI16 ClipDepth;
protected:
	TagPlaceObject2()
		: flags1(0)
		, Character(nullptr)
		, Ratio(0)
		, ClipDepth(0)
	{

	}
public:
	CREATE_FUNC(TagPlaceObject2);
	void visit(TagHandler* h) override;
public:
	inline bool hasClipAction()
	{
		return (flags1 & HAS_CLIP_ACTION) != 0;
	}

	inline bool hasClipDepth()
	{
		return (flags1 & HAS_CLIP_DEPTH) != 0;
	}

	inline void setClipDepth(UI16 depth)
	{
		this->ClipDepth = depth;
		flags1 |= HAS_CLIP_DEPTH;
	}

	inline bool hasMatrix()
	{
		return (flags1 & HAS_MATRIX) != 0;
	}

	inline void setMatrix(const Matrix& matrix)
	{
		this->matrix = matrix;
		flags1 |= HAS_MATRIX;
	}

	inline bool hasCxform()
	{
		return (flags1 & HAS_CXFORM) != 0;
	}

	inline bool hasName()
	{
		return (flags1 & HAS_NAME) != 0;
	}

	inline bool hasRatio()
	{
		return (flags1 & HAS_RATIO) != 0;
	}

	inline bool hasMove()
	{
		return (flags1 & HAS_MOVE) != 0;
	}

	inline bool hasCharacter()
	{
		return (flags1 & HAS_CHARACTER) != 0;
	}

	inline void setCharacter(SWFTagRef* tag)
	{
		this->removeReference(this->Character);
		this->Character = tag;
		this->putReference(tag);
		if (tag == nullptr)
			flags1 &= ~HAS_CHARACTER;
		else
			flags1 |= HAS_CHARACTER;
	}
};

class TagPlaceObject3
	: public TagPlaceObject2
{
public:
	UI8 flags2;

	std::string ClassName;
	FilterList SurfaceFilterList;
	UI8 BlendMode;
	UI8 BitmapCache;
	UI8 Visible;
	ColorRGBA BackgroundColor;

	//TODO
	ByteArray remainData;
protected:
	TagPlaceObject3();
public:
	CREATE_FUNC(TagPlaceObject3);

	inline bool hasBlendMode()
	{
		return (flags2 & HAS_BLEND_MODE) != 0;
	}

	inline bool hasCacheAsBitmap()
	{
		return (flags2 & HAS_CACHE_AS_BITMAP) != 0;
	}

	inline bool hasImage()
	{
		return (flags2 & HAS_IMAGE) != 0;
	}

	inline bool hasClassName()
	{
		return (flags2 & HAS_CLASS_NAME) != 0;
	}

	inline bool hasFilterList()
	{
		return (flags2 & HAS_FILTER_LIST) != 0;
	}
public:
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};

class TagRemoveObject2
	: public SWFTag
{
protected:
	TagRemoveObject2(){};
public:
	CREATE_FUNC(TagRemoveObject2);
	void visit(TagHandler* h) override;
};

class TagRemoveObject
	: public SWFTag
{
protected:
	TagRemoveObject(){};
public:
	CREATE_FUNC(TagRemoveObject);
	void visit(TagHandler* h) override;
};
#pragma endregion The Display List

#pragma region Images
class TagDefineBits
	: public DefineTag
{
public:
	ByteArray imageData;
protected:
	TagDefineBits(){};
public:
	CREATE_FUNC(TagDefineBits);
	void visit(TagHandler* h) override;
};

class TagDefineBitsJPEG2
	: public TagDefineBits
{
protected:
	TagDefineBitsJPEG2(){};
public:
	CREATE_FUNC(TagDefineBitsJPEG2);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};
class TagDefineBitsJPEG3
	: public TagDefineBitsJPEG2
{
public:
	ByteArray alphaData;
protected:
	TagDefineBitsJPEG3(){};
public:
	CREATE_FUNC(TagDefineBitsJPEG3);
	void visit(TagHandler* h) override;
	void setData(char* data, size_t len) override;
};
class TagDefineBitsLossless
	: public TagDefineBits
{
public:
	UI8 BitmapFormat;
	UI16 BitmapWidth;
	UI16 BitmapHeight;
	UI8 BitmapColorTableSize;
	ByteArray ZlibBitmapData;
protected:
	TagDefineBitsLossless()
		: BitmapFormat(0)
		, BitmapWidth(0)
		, BitmapHeight(0)
	{

	}
public:
	CREATE_FUNC(TagDefineBitsLossless);
	void visit(TagHandler* handler) override;
};
class TagDefineBitsLossless2
	: public TagDefineBitsLossless
{
protected:
	TagDefineBitsLossless2(){};
public:
	CREATE_FUNC(TagDefineBitsLossless2);
	void visit(TagHandler* handler) override;
};
#pragma endregion Images

#pragma region Meta data
class TagDefineBinaryData : public DefineTag
{
public:
	ByteArray binary;
protected:
	TagDefineBinaryData(){};
public:
	CREATE_FUNC(TagDefineBinaryData);
	void visit(TagHandler* h) override;
};
#pragma endregion Meta data

class TagUnknow : public SWFTag
{
protected:
	TagUnknow();
public:
	CREATE_FUNC(TagUnknow);
	void visit(TagHandler* h) override;
	std::string getTagName() const override;
};
#endif // SWFTag_h__
