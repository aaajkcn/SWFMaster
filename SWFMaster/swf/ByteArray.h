#ifndef ByteArray_h__
#define ByteArray_h__

class ByteArray
{
private:
	char* data;
	size_t len;
	size_t allocLength;
public:
	ByteArray(char* data, size_t len);
	ByteArray(size_t len);
	ByteArray();

	~ByteArray();

	void resize(size_t size);
	void clear();
	void copy(const char* data, size_t len, size_t offset = 0);

	inline char* getData() const { return data; }
	inline size_t getLength() const { return len; }
};
#endif // ByteArray_h__
