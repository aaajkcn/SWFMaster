#-------------------------------------------------
#
# Project created by QtCreator 2014-04-04T00:59:37
#
#-------------------------------------------------

QT       += core gui axcontainer

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SWFMaster
TEMPLATE = app

INCLUDEPATH += .
INCLUDEPATH += third-party
INCLUDEPATH += third-party/hexedit

SOURCES += main.cpp\
        SWFMasterWindow.cpp \
    swf/SWF.cpp \
    swf/SWFInputStream.cpp \
    swf/SWFTag.cpp \
    swf/SWFTagFactory.cpp \
    swf/tags/TagDoABC.cpp \
    swf/tags/TagMetadata.cpp \
    swf/tags/TagUnknow.cpp \
    swf/abc/ABCFile.cpp \
    swf/abc/Opcode.cpp \
    editor/model/DoABCTreeItem.cpp \
    editor/MetaDataEditor.cpp \
    editor/DoABCEditor.cpp \
    editor/Editor.cpp \
    editor/OpcodeEditor.cpp \
    editor/SelectExternalTagWindow.cpp \
    editor/model/OpcodeItem.cpp \
    editor/model/SWFTreeModel.cpp \
    editor/model/TagTreeItem.cpp \
    swf/ByteArray.cpp \
    swf/Object.cpp \
    swf/SWFDecoder.cpp \
    swf/SWFEncoder.cpp \
    swf/SWFOutputStream.cpp \
    swf/SWFStructs.cpp \
    swf/SWFTagWrapper.cpp \
    swf/TagDecoder.cpp \
    swf/abc/AS3Model.cpp \
    swf/tags/TagCSMTextSettings.cpp \
    swf/tags/TagDefineBitsJPEG2.cpp \
    swf/tags/TagDefineBitsJPEG3.cpp \
    swf/tags/TagDefineButton.cpp \
    swf/tags/TagDefineButton2.cpp \
    swf/tags/TagDefineButtonCxform.cpp \
    swf/tags/TagDefineButtonSound.cpp \
    swf/tags/TagDefineEditText.cpp \
    swf/tags/TagDefineFont.cpp \
    swf/tags/TagDefineFont2.cpp \
    swf/tags/TagDefineFont3.cpp \
    swf/tags/TagDefineFont4.cpp \
    swf/tags/TagDefineFontAlignZones.cpp \
    swf/tags/TagDefineFontInfo.cpp \
    swf/tags/TagDefineFontInfo2.cpp \
    swf/tags/TagDefineFontName.cpp \
    swf/tags/TagDefineMorphShape.cpp \
    swf/tags/TagDefineMorphShape2.cpp \
    swf/tags/TagDefineScalingGrid.cpp \
    swf/tags/TagDefineSceneAndFrameLabelData.cpp \
    swf/tags/TagDefineShape.cpp \
    swf/tags/TagDefineShape2.cpp \
    swf/tags/TagDefineShape3.cpp \
    swf/tags/TagDefineShape4.cpp \
    swf/tags/TagDefineSprite.cpp \
    swf/tags/TagDefineText.cpp \
    swf/tags/TagDefineText2.cpp \
    swf/tags/TagEnableDebugger.cpp \
    swf/tags/TagEnableDebugger2.cpp \
    swf/tags/TagEnd.cpp \
    swf/tags/TagExportAssets.cpp \
    swf/tags/TagFileAttributes.cpp \
    swf/tags/TagFrameLabel.cpp \
    swf/tags/TagImportAssets2.cpp \
    swf/tags/TagPlaceObject.cpp \
    swf/tags/TagPlaceObject2.cpp \
    swf/tags/TagPlaceObject3.cpp \
    swf/tags/TagProtect.cpp \
    swf/tags/TagScriptLimits.cpp \
    swf/tags/TagSetBackgroundColor.cpp \
    swf/tags/TagSetTabIndex.cpp \
    swf/tags/TagShowFrame.cpp \
    swf/tags/TagSymbolClass.cpp \
    swf/builder/TagBuilder.cpp \
    swf/utils/ImageHelper.cpp \
    third-party/lzma/Alloc.c \
    third-party/lzma/Bcj2.c \
    third-party/lzma/Bra.c \
    third-party/lzma/Bra86.c \
    third-party/lzma/BraIA64.c \
    third-party/lzma/CpuArch.c \
    third-party/lzma/Delta.c \
    third-party/lzma/LzFind.c \
    third-party/lzma/LzFindMt.c \
    third-party/lzma/Lzma2Dec.c \
    third-party/lzma/Lzma2Enc.c \
    third-party/lzma/Lzma86Dec.c \
    third-party/lzma/Lzma86Enc.c \
    third-party/lzma/LzmaDec.c \
    third-party/lzma/LzmaEnc.c \
    third-party/lzma/LzmaLib.c \
    third-party/lzma/MtCoder.c \
    third-party/lzma/Ppmd7.c \
    third-party/lzma/Ppmd7Dec.c \
    third-party/lzma/Ppmd7Enc.c \
    third-party/lzma/Sha256.c \
    third-party/lzma/Threads.c \
    third-party/hexedit/commands.cpp \
    third-party/hexedit/qhexedit.cpp \
    third-party/hexedit/qhexedit_p.cpp \
    third-party/hexedit/xbytearray.cpp \
    editor/EditorContainer.cpp \
    editor/Icon.cpp \
    editor/tags/DefineBitsEditor.cpp \
    editor/tags/DisplayTagEditor.cpp

HEADERS  += SWFMasterWindow.h \
    swf/SWF.h \
    swf/SWFInputStream.h \
    swf/SWFTag.h \
    swf/SWFTagFactory.h \
    swf/SWFTypes.h \
    swf/tags/TagDoABC.h \
    swf/abc/ABCFile.h \
    swf/abc/Opcode.h \
    editor/model/DoABCTreeItem.h \
    editor/MetaDataEditor.h \
    editor/DoABCEditor.h \
    editor/Editor.h \
    editor/OpcodeEditor.h \
    editor/EditorContainer.h \
    editor/Icon.h \
    editor/OpCodeEditor.h \
    editor/SelectExternalTagWindow.h \
    editor/model/OpcodeItem.h \
    editor/model/SWFTreeModel.h \
    editor/model/TagTreeItem.h \
    swf/ByteArray.h \
    swf/Object.h \
    swf/SWFDecoder.h \
    swf/SWFEncoder.h \
    swf/SWFOutputStream.h \
    swf/SWFStructs.h \
    swf/SWFTagWrapper.h \
    swf/TagDecoder.h \
    swf/TagHandler.h \
    swf/abc/AS3Model.h \
    swf/builder/TagBuilder.h \
    swf/utils/ImageHelper.h \
    third-party/lzma/Alloc.h \
    third-party/lzma/Bcj2.h \
    third-party/lzma/Bra.h \
    third-party/lzma/CpuArch.h \
    third-party/lzma/Delta.h \
    third-party/lzma/LzFind.h \
    third-party/lzma/LzFindMt.h \
    third-party/lzma/LzHash.h \
    third-party/lzma/Lzma2Dec.h \
    third-party/lzma/Lzma2Enc.h \
    third-party/lzma/Lzma86.h \
    third-party/lzma/LzmaDec.h \
    third-party/lzma/LzmaEnc.h \
    third-party/lzma/LzmaLib.h \
    third-party/lzma/MtCoder.h \
    third-party/lzma/Ppmd.h \
    third-party/lzma/Ppmd7.h \
    third-party/lzma/RotateDefs.h \
    third-party/lzma/Sha256.h \
    third-party/lzma/Threads.h \
    third-party/lzma/Types.h \
    third-party/hexedit/commands.h \
    third-party/hexedit/qhexedit.h \
    third-party/hexedit/qhexedit_p.h \
    third-party/hexedit/xbytearray.h \
    editor/tags/DefineBitsEditor.h \
    editor/tags/DisplayTagEditor.h

FORMS    += SWFMasterWindow.ui \
    editor/MetaDataEditor.ui \
    editor/OpcodeEditor.ui \
    editor/SelectExternalTagWindow.ui \
    editor/tags/DefineBitsEditor.ui \
    editor/tags/DisplayTagEditor.ui

RESOURCES += \
    icons.qrc

OTHER_FILES += \
    resources/style.css \
    resources/darkOrange.css
