#ifndef OPCODEEDITOR_H
#define OPCODEEDITOR_H

#include <QWidget>
#include "swf/abc/ABCFile.h"
#include "Editor.h"

class SWFTreeModel;
class AS3Method;

namespace Ui {
class OpcodeEditor;
}

class OpcodeEditor
	: public QWidget
	, public Editor
{
    Q_OBJECT

public:
    explicit OpcodeEditor(AS3Method* mb, QWidget *parent = 0);
	~OpcodeEditor();
	const QString getEditorName() const override;
	QWidget* asWidget() const override;
private slots:
	void onContextMenuRequested(const QPoint&);
private:
    Ui::OpcodeEditor *ui;
	SWFTreeModel* _model;
	AS3Method* _method;
};

#endif // OPCODEEDITOR_H
