#ifndef EditorContainer_h__
#define EditorContainer_h__

#include <QWidget>
#include <QTabWidget>
#include <map>

class SWFTreeItem;
class Editor;

class EditorContainer
	: public QWidget
{
	Q_OBJECT
private:
	QTabWidget* _tabView;
	std::map<void*, Editor*> _editorMap;
public:
	EditorContainer();
	Editor* openEditor(SWFTreeItem* source);
private slots:
	void onTabCloseReq(int);
private:
	void init();

};
#endif // EditorContainer_h__
