#include "MetaDataEditor.h"
#include "ui_MetaDataEditor.h"

MetaDataEditor::MetaDataEditor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MetaDataEditor)
{
    ui->setupUi(this);
}

MetaDataEditor::~MetaDataEditor()
{
    delete ui;
}
