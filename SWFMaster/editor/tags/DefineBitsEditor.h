#ifndef DEFINEBITSJPEG3EDITOR_H
#define DEFINEBITSJPEG3EDITOR_H

#include <QWidget>
#include "swf/SWFTag.h"
#include "../Editor.h"

namespace Ui {
class DefineBitsEditor;
}

class SWFTagWrapper;

class DefineBitsEditor
	: public QWidget
	, public Editor
{
    Q_OBJECT

public:
    explicit DefineBitsEditor(SWFTagWrapper* wrapper, QWidget *parent = 0);
	~DefineBitsEditor();
	virtual const QString getEditorName() const override;
	virtual QWidget* asWidget() const override;
private slots:
	void onReplace();
	void onExport();
private:
    Ui::DefineBitsEditor *ui;
	SWFTagWrapper* _wrapper;
	TagDefineBits* _bits;
	QImage* _image;
	QGraphicsPixmapItem* _graphicsPixmapItem;
};

#endif // DEFINEBITSJPEG3EDITOR_H
