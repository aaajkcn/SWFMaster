#ifndef SWFTreeModel_h__
#define SWFTreeModel_h__

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QMenu>

class AS3SWF;
class SWF;
class SWFTreeItem;

enum class SWFTreeItemKind
{
	Unknow,
	DoABC,
	Cpool,
	Instance,
	Method,
	Tag
};

class SWFTreeModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	SWFTreeModel(QObject *parent = 0);
	~SWFTreeModel();

	QVariant data(const QModelIndex &index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	Qt::ItemFlags flags(const QModelIndex &index) const;
	QModelIndex index(int row, int column,
		const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	bool hasChildren(const QModelIndex &parent  = QModelIndex()) const override;
public:
	void appendRow(SWFTreeItem* item);
	SWFTreeItem* itemFromIndex(const QModelIndex& index) const;
	void setHeaderStringList(QStringList& list);
private:
	SWFTreeItem* _rootItem;
	QStringList _headerStringList;
};

class SWFTreeItem
{
	friend class SWFTreeModel;
public:
	SWFTreeItem(const std::string& data = std::string());
	~SWFTreeItem();

	void appendRow(SWFTreeItem *child);

	int columnCount() const;
	int row() const;

	virtual QVariant data(int column, int role) const;
	virtual void setData(const QVariant& var, int role = Qt::DisplayRole, int column = 0);
	
	SWFTreeItem* child(int row);
	SWFTreeItem* parent();

	void setName(const std::string& name);
	void setName(const QString& name);
	void setIcon(const QIcon& icon);
	
	virtual void beforeRequestChildCount();
	virtual int childCount() const;
	virtual bool hasChildren() const;
	virtual QMenu* requestContextMenu();

	inline SWFTreeItemKind getKind() const { return _kind; }
	inline void setKind(SWFTreeItemKind kind) { _kind = kind; }

	inline void setUserData(void* ptr) { _dataPtr = ptr; }
	inline void* getUserData() const { return _dataPtr; }
protected:
	QList<SWFTreeItem*> _childItems;
	QMap<int, QMap<int, QVariant> > _varMap;
	SWFTreeItemKind _kind;
	SWFTreeItem* _parentItem;
	SWFTreeModel* _model;
	void* _dataPtr;
};

class SWFItem
	: public SWFTreeItem
{
private:
	AS3SWF* _as3swf;
public:
	SWFItem(AS3SWF* as3swf);
};
#endif // SWFTreeModel_h__
