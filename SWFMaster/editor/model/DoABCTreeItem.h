#ifndef DOABCTREEITEM_H
#define DOABCTREEITEM_H

#include "editor/model/SWFTreeModel.h"
#include "swf/tags/TagDoABC.h"
#include "swf/abc/ABCFile.h"
#include "swf/abc/AS3Model.h"

class SWFClassItem;
class CpoolInfoItem;

class DoABCTreeItem
        : public SWFTreeItem
{
public:
    DoABCTreeItem(AS3ABCFile* abc);
	inline TagDoABC* getTagABC() const { return nullptr; }
	bool hasChildren() const override;
	void beforeRequestChildCount() override;
	QMenu* requestContextMenu() override;
private:
    AS3ABCFile* _abc;
	bool _categoryInit;
	SWFClassItem* _classItem;
	CpoolInfoItem* _poolItem;
};

class SWFClassItem
	: public SWFTreeItem
{
private:
	AS3Class* _clazz;
public:
	SWFClassItem(AS3Class*);
public:
	void beforeRequestChildCount() override;
	bool hasChildren() const override;
};

class CpoolInfoItem
	: public SWFTreeItem
{
private:
	CpoolInfo* _poolInfo;
public:
	CpoolInfoItem(CpoolInfo* pool);
public:
	bool hasChildren() const override;
};
#endif // DOABCTREEITEM_H
