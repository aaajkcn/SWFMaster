#ifndef OpcodeItem_h__
#define OpcodeItem_h__

#include "SWFTreeModel.h"

class OpcodeInstruction;
class CpoolInfo;

class OpcodeItem
	: public SWFTreeItem
{
private:
	CpoolInfo* _pool;
	OpcodeInstruction* _instruction;
public:
	OpcodeItem* prev;
	OpcodeItem* next;
public:
	OpcodeItem(OpcodeInstruction*, CpoolInfo*);
	void setOffset(size_t offset);
private:
	void init();
};
#endif // OpcodeItem_h__
