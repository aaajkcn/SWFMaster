#ifndef METADATAEDITOR_H
#define METADATAEDITOR_H

#include <QWidget>

namespace Ui {
class MetaDataEditor;
}

class MetaDataEditor : public QWidget
{
    Q_OBJECT

public:
    explicit MetaDataEditor(QWidget *parent = 0);
    ~MetaDataEditor();

private:
    Ui::MetaDataEditor *ui;
};

#endif // METADATAEDITOR_H
